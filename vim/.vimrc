" When started as "evim", evim.vim will already have done these settings.
if v:progname =~? "evim"
  finish
endif

" Bail out if something that ran earlier, e.g. a system wide vimrc, does not
" want Vim to use these default values.
if exists('skip_defaults_vim')
  finish
endif

" Use Vim settings, rather than Vi settings (much better!).
" This must be first, because it changes other options as a side effect.
" Avoid side effects when it was already reset.
if &compatible
  set nocompatible
endif

" When the +eval feature is missing, the set command above will be skipped.
" Use a trick to reset compatible only when the +eval feature is missing.
silent! while 0
  set nocompatible
silent! endwhile

" Allow backspacing over everything in insert mode.
set backspace=indent,eol,start

set history=200		" keep 200 lines of command line history
set ruler		" show the cursor position all the time
set showcmd		" display incomplete commands
set wildmenu		" display completion matches in a status line

set ttimeout		" time out for key codes
set ttimeoutlen=100	" wait up to 100ms after Esc for special key

" Show @@@ in the last line if it is truncated.
set display=truncate

" Show a few lines of context around the cursor.  Note that this makes the
" text scroll if you mouse-click near the start or end of the window.

syntax on

colorscheme desert

" ------------------ general config ------------------
" display line number
 set number
" set relativenumber "OH BOy !

" make split open below the active one  
set splitbelow

" change term buff size
set termwinsize=10x0

" re-read file
set autoread

" ignore file when opening by match
set wildignore+=.pyc,.swp


" ------------------ indentation config ------------------
set autoindent
set expandtab
set shiftround
set shiftwidth=4
set smarttab
set tabstop=4
set smartindent

" ------------------ Search options ------------------
set hlsearch
set incsearch
hi Search ctermbg=Cyan
hi Search cterm=bold
hi Search ctermfg=black


" ------------------ Text display options ------------------
set scrolloff=3
set sidescrolloff=5
set linebreak
set wrap


" ------------------  UI options ------------------
set laststatus=2
set ruler
"set cursorline
set title


" ------------------ Code floding options ------------------
set foldmethod=indent
set foldnestmax=1
set nofoldenable


" ------------------ netrw config ------------------
" change netrw directory display to tree
let g:netrw_liststyle = 3

" removing netrw top banner
let g:netrw_banner = 0

" open file in new tab
let g:netrw_browse_split = 3

" set netrw size
let g:netrw_winsize = 10

" let g:netrw_keepdir = 0

" ------------------ custom shortcuts ------------------

nnoremap <C-t> :term<CR>
nnoremap <S-z><S-a> :wqa<CR>
nnoremap <F3> :r!date "+\%F \%T"<CR>
nnoremap <F2> :w !sudo tee %<CR>
nnoremap <F1> :Vexplore<CR>

" Focus function, in order :
" (1) close buffers,
" (2) reopen last open
" (3) remove line number so copy/paste won't be parasited by leading numbers 
" Note : (1) +  (2) is a trick to only keep the active buffer"
function Focus()
     :%bd
     :e#
     :set nonumber
endfunction

nnoremap <S-f> :call Focus()<CR>

" exiting insert mode 
inoremap <C-i> <ESC>
nnoremap <C-i> :star<CR>

" navigating through split buffers
nmap <C-h> <C-w>h
nmap <C-j> <C-w>j
nmap <C-k> <C-w>k
nmap <C-l> <C-w>l

" --------------- Insert mode display ----------------------
":autocmd InsertEnter * set cursorline
":autocmd InsertLeave * set nocul

set cul
highlight CursorLine cterm=Bold ctermbg=None
:autocmd InsertEnter * highlight CursorLine cterm=italic,Bold ctermbg=Black 
:autocmd InsertLeave * highlight CursorLine cterm=Bold ctermbg=None


" ------------------ startup buffer config ------------------

augroup ProjectDrawer
  autocmd!
  " autocmd VimEnter * :Vexplore | wincmd w
  " autocmd BufNew * :Vexplore | wincmd w
  " autocmd tabNew * :Vexplore | wincmd w
  "autocmd VimEnter * :term | wincmd w
augroup END

augroup BgHighlight
    autocmd!
    highlight StatusLineNC cterm=bold ctermfg=white ctermbg=darkgray
augroup END


" ---------------- fixing last position non-persistence --------------------
if has("autocmd")
  au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
endif


